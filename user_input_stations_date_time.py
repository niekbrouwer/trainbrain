import datetime
import csv

def search_station1(filename):
    station1 = ""
    found = False
    while not found:

        print("What will be your departure station:")

        station1 = input()

        with open(filename) as data:
            reader = csv.DictReader(data)
            for line in reader:
                #print(line)
                if station1 in line["tussenstops"]:

                    found = True
        if len(station1) < 4:
            found = False
        if not found:
            print("This is not a station we know, try again or type quit")



    return station1


def search_station2(filename):
    station2 = ""
    found = False
    while not found:

        print("What will be your arrival station:")

        station2 = input()

        with open(filename) as data:
            reader = csv.DictReader(data)
            for line in reader:

                if station2 in line["tussenstops"]:

                    found = True
        if len(station2) < 4:
            found = False
        if not found:
            print("This is not a station we know, try again or type quit")

    return station2

def time_date():
    time = ""

    isValidDate = False
    while not time or len(str(time))!= 4 or int(time) > 2359 or int(time) < 0000:
        print("What time do you want to leave:")
        try:
            time = int(input())

        except ValueError:
            print("The time needs to be in the following format: ####")
        if time:
            if len(str(time)) == 3:
                time = "0" + str(time)
            if len(str(time))!= 4:
                print("Please input your time in the following format: ####")
            elif int(time) > 2359 or int(time) < 0000:
                print("This is not a valid time, try again")

    while not isValidDate:
        print("Please enter your day of travelling in the following format: 'dd/mm/yyyy':")
        inputDate = input()
        day, month, year = inputDate.split('/')


        isValidDate = True
        try:
            datetime.datetime(int(year), int(month), int(day))
        except ValueError:
            isValidDate = False
            print("Please enter a valid date")




    return time, inputDate





