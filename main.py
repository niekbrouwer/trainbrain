import os
import csv

from Developer_code.model import load_data, weights, generate_dates, prob, generate_chance
from Developer_code.trajects_to_csv import run_main
from Developer_code.user_input_stations_date_time import search_station1, search_station2, time_date
from Developer_code.generate_intermediate_stops import run
from Developer_code.stations_to_trajects import all_trajectories


def check_file(filename, dictionary):
    if not os.path.exists(filename):
        write_to_file(dictionary(), filename)

    return


def write_to_file(dictionary, filename):
    csv_columns = ["name", "tussenstops"]
    with open(filename, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for data in dictionary:
            writer.writerow(data)


def search(filename):
    station1 = search_station1(filename)
    station2 = search_station2(filename)
    time, date = time_date()

    if station1:
        print(f'station1 = {station1}')
    if station2:
        print(f'station2 = {station2}')
    if time:
        time = str(time)
        print(f'time = {time}')
    if date:
        day, month, year = date.split('/')
        date = str(year) + "-" + str(month) + "-" + str(day)


    return [station1, station2, time, date]


def statistics_input(filename):
    INPUT = search(filename)
    # INPUT = ["Den Haag HS",
    #          "Sittard",
    #          '09:00',
    #          '2020-02-21']
    tussenstops = run(INPUT)
    iets = all_trajectories(INPUT, filename, tussenstops)

    return iets, INPUT[3]


file1 = "dataset_3.0_2017.csv"
file2 = "dataset_3.0_2018.csv"
file3 = "dataset_3.0_2019.csv"
filename = "tussenstations.csv"
check_file(filename, run_main)
trajecten, datum = statistics_input(filename)
print(trajecten)
input()
date = generate_dates(datum)
print(date)
input()
array = (load_data(file1, file2, file3, date, trajecten))
file_freq = "trajectory_frequencies.csv"
weight = weights(array)
all, array = prob(weight, trajecten, file_freq)

kans = generate_chance(all, array)
input()
print(kans)

