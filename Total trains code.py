import requests
import csv
from bs4 import BeautifulSoup
import pandas as pd
from generate_intermediate_stops import run
from stations_to_trajects import opnieuw
import html5lib

url = 'https://wiki.ovinnederland.nl/wiki/Dienstregeling_2020'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html5lib')

table = soup.find('table', {'class':'wikitable sortable'}).tbody
rows = table.find_all('tr')
columns = [v.text.replace('\n', '') for v in rows[0].find_all('th')]
dic = {}
for i in range(1, len(rows)):
    tds = rows[i].find_all('td')
    values = [td.text.replace('\n', '') for td in tds]
    dic[values[3]] = values[2]


def find_frequency(trajectory):
    try:
        return dic[trajectory]
    except KeyError:
        return 'This trajectory does not exist'


def createdataset():
    trajectory_list = []
    with open("tussenstations.csv") as f:

        reader = csv.DictReader(f)
        for row in reader:
            stations = row['name'].split(" - ")
            trajectory_list.append(stations[0] + '-' + stations[1])

        data_freq = pd.DataFrame(index=trajectory_list, columns=['frequency'])
        for i in range(len(trajectory_list)):
                data_freq.iloc[i][0] = 0

    return data_freq


def fill_freq_data(dataset, dic):
    for traj in dic:
        initial_traj = traj

        if 'dag' in dic[initial_traj] or 'week' in dic[initial_traj] or 'nacht' in dic[initial_traj]:
            continue
        if 'Weener' in traj or 'Bielefeld' in traj:
            continue

        # 404 error on 9292.nl
        if 'Dordrecht' in traj and 'Den Haag Centraal' in traj:
            continue

        if 'Utrecht Maliebaan' in traj:
            traj = traj.replace('Utrecht Maliebaan', 'Maliebaan Utrecht')
        if '/' in traj:
            traj = traj.replace('/', ' - ')
        if 'ü' in traj:
            traj = traj.replace('ü', 'u')
        if '(M)' in traj:
            traj = traj.replace('(M)', '')
        if '(' in traj and ')' in traj and '(M)' not in traj and '(Westf.)' not in traj:
            traj = traj.replace('(', '')
            traj = traj.replace(')', '')
        if '→' in traj:
            traj = traj.replace('→', '-')
        if '←' in traj:
            traj = traj.replace('←', '-')
        if 'Alphen aan den Rijn' in traj:
            traj = traj.replace('Alphen aan den Rijn', 'Alphen a d Rijn')
        if 'Brussel-Zuid' in traj:
            traj = traj.replace('Brussel-Zuid', 'Brussel Zuid Midi')
        if 'Arnhem' in traj:
            traj = traj.replace('Arnhem Centraal', 'Arnhem')
        if 'Eindhoven' in traj:
            traj = traj.replace('Eindhoven Centraal', 'Eindhoven')
        if 'Amersfoort' in traj:
            traj = traj.replace('Amersfoort Centraal', 'Amersfoort')
        if "'s-Hertogenbosch" in traj:
            traj = traj.replace("'s-Hertogenbosch", "Hertogenbosch")
        if 'Westf.' in traj:
            traj = traj.replace('Westf.', '')
            traj = traj.replace('Munster', 'Muenster')

        stations = traj.split(' - ')
        i = 0
        while i < len(stations) - 1:
            stat1, stat2 = stations[i], stations[i+1]
            INPUT = [stat1,
            stat2,
            "1200",
            "2020-01-20"]
            try:
                tussenstops = run(INPUT)
            except UnboundLocalError:
                i += 1
                continue
            filename = "tussenstations.csv"

            #print(INPUT)
            #print(tussenstops)

            try:
                trajecten = opnieuw(INPUT, filename, tussenstops)
            #print(trajecten)
            except IndexError:
                i += 1
                continue

            new_trajects = []
            for traject in trajecten:
                splitted = traject.split(" - ")
                combined = splitted[0] + "-" + splitted[1]
                new_trajects.append(combined)

            freq = dic[initial_traj].split('x')[0]
            if '-' in freq:
                mul_freq = freq.split('-')
                freq = (int(mul_freq[0]) + int(mul_freq[1]))/2
            else:
                freq = int(freq)

            for newtraj in new_trajects:
                print(newtraj)
                dataset.at[newtraj, 'frequency'] += 18 * freq

            i += 1

    return dataset


def create_csv_file_frequencies(dic):
    dataset = fill_freq_data(createdataset(), dic)
    filename = 'trajectory_frequencies.csv'
    dataset.to_csv(filename)


create_csv_file_frequencies(dic)

