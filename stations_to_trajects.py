# Tijdelijk:
import csv


def make_trajectory_dict(INPUT, filename):
    with open(filename) as csv_file:
        reader = csv.DictReader(csv_file)
        trajecten_station = {}
        station1 = INPUT[0]

        station2 = INPUT[1]
        if station2 == "Dusseldorf Hbf":
            station2 = "Düsseldorf Hbf"
        if station2 == "Muenster  Hbf":
            station2 = "Münster (Westf.) Hbf"
        stations1 = []
        stations2 = []
        for line in reader:

            if station1 in line["tussenstops"]:
                stations1.append(line)
            if station2 is not None:
                if station2 in line["tussenstops"]:
                    stations2.append(line)

        trajecten_station[station1] = stations1
        if len(stations2) != 0:
            trajecten_station[station2] = stations2

    return trajecten_station


def match_count(dict, stops):
    stops = stops[39:-2].split(",")
    stops_list = []
    for stop in stops:
        if 's Hertogenbosch' in stop:
            stops_list.append("'" + str(stop[3:]))
        elif 'Schiphol' in stop:
            stops_list.append("Schiphol Airport")
        elif 'Duesseldorf Hbf' in stop:
            stops_list.append("Düsseldorf Hbf")
        elif 'Muenster (Westfalen) Hbf' in stop:
            stops_list.append("Münster (Westf.) Hbf")
        else:
            stops_list.append(stop)
    spoor = ""
    score_list = []

    trajectory_list = []
    for eindstation in dict:
        high_score = 0
        for trajectory in dict[eindstation]:
            score = 0
            tussenstops = trajectory["tussenstops"]
            for stop in stops_list:
                if stop in tussenstops:
                    score += 1
            if score >= high_score:
                spoor = trajectory["name"]
                high_score = score

        trajectory_list.append(spoor)

        score_list.append(high_score)
    return score_list, trajectory_list


def all_trajectories(INPUT, filename, tussenstops):
    print(tussenstops)
    def secondary_tussenstops(tussenstops):
        tussenstops = tussenstops[39:-2].split(",")
        trd_tus = []
        for stop in tussenstops:
            if stop == "'Amersfoort Centraal'":
                stop = "'Amersfoort'"
            if stop == "'s Hertogenbosch'":
                stop = "'Hertogenbosch'"
            if stop == "'Eindhoven Centraal'":
                stop = "'Eindhoven'"
            if stop == "'Schiphol'":
                stop = "'Schiphol Airport'"
            if stop == "'Duesseldorf Hbf'":
                stop = "'Düsseldorf Hbf'"
            trd_tus.append(stop)
        sec_tus = []

        for i in range(1, 12):
            for stop in range(len(trd_tus)-i):
                sec_tus.append(str(str(trd_tus[stop])[1:-1] + " - " + str(trd_tus[stop + i])[1:-1]))
                sec_tus.append(str(str(trd_tus[stop + i])[1:-1] + " - " + str(trd_tus[stop])[1:-1]))

        return sec_tus

    sec_tus = secondary_tussenstops(tussenstops)
    eindlijst_trajecten = []
    trajectory_list = match_count(make_trajectory_dict(INPUT, filename), tussenstops)[1]
    #print(trajectory_list)
    traject1 = trajectory_list[0]
    traject2 = trajectory_list[1]
    running = True

    def aparte_stations(traject):
        station1 = traject.split(" - ")[0]
        station2 = traject.split(" - ")[1]
        return station1, station2

    station1 = aparte_stations(traject1)[0]
    station2 = aparte_stations(traject1)[1]
    station3 = aparte_stations(traject2)[0]
    station4 = aparte_stations(traject2)[1]
    lst = [station1, station2, station3, station4]

    def match(traject1, traject2, keer):
        continu = True
        if not keer:
            if traject1 == traject2:
                eindlijst_trajecten.append((traject1))
                continu = False


            elif station1 == station3 \
                    or station1 == station4 \
                    or station2 == station3 \
                    or station2 == station4:
                eindlijst_trajecten.append(traject1)
                eindlijst_trajecten.append(traject2)
                continu = False
            else:


                eindlijst_trajecten.append(traject1)
                eindlijst_trajecten.append(traject2)
        else:
            #print(traject1)
            #print(traject2)
            if traject1[0] == traject2:
                eindlijst_trajecten.append((traject1))
                continu = False


            elif traject1[0].split(" - ")[0] == traject2[0].split(" - ")[0] \
                    or traject1[0].split(" - ")[0] == traject2[0].split(" - ")[1] \
                    or traject1[0].split(" - ")[1] == traject2[0].split(" - ")[0] \
                    or traject1[0].split(" - ")[1] == traject2[0].split(" - ")[1]:

                continu = False
            else:
                pass


        return continu

    if not match(traject1, traject2, False):
        return eindlijst_trajecten


    second_lst = []
    for item in lst:
        if item == INPUT[0] or item == INPUT[1]:
            pass
        else:
            second_lst.append(item)
    third_lst = []

    for item in second_lst:
        new_traj = make_trajectory_dict([item, None], filename)

        for traject in new_traj[item]:
            if traject["name"] in eindlijst_trajecten:
                new_traj[item].remove(traject)

        if match_count(new_traj, tussenstops)[0][0] >= 2:
            third_lst.append(item)


    while running:
        #print(eindlijst_trajecten)
        fourth_lst = []
        #print(sec_tus)
        for item in third_lst:
            #print(item)
            new_traj = make_trajectory_dict([item, None], filename)
            #print(new_traj[item])
            for traject in new_traj[item]:
                if traject["name"] in eindlijst_trajecten:
                    new_traj[item].remove(traject)
            #         #print(traject)
            # print(match_count(new_traj, tussenstops)[0][0])
            # print(match_count(new_traj, tussenstops)[1][0])

            if match_count(new_traj, tussenstops)[0][0] >= 2 and match_count(new_traj, tussenstops)[1][0] in sec_tus:
                fourth_lst.append(item)
            else:
                #print(item)
                for traject in new_traj[item]:
                    #print(traject["name"])
                    if traject["name"] == match_count(new_traj, tussenstops)[1][0]:
                        new_traj[item].remove(traject)
                #         #print(traject)
                # print(match_count(new_traj, tussenstops)[0][0])
                # print(match_count(new_traj, tussenstops)[1][0])
                if match_count(new_traj, tussenstops)[0][0] >= 2 and match_count(new_traj, tussenstops)[1][0] in sec_tus:
                    fourth_lst.append(item)
                else:
                    # print(match_count(new_traj, tussenstops)[0][0])
                    # #print(match_count(new_traj, tussenstops)[1][0])
                    for traject in new_traj[item]:
                        #print(traject)
                        if traject["name"] == match_count(new_traj, tussenstops)[1][0]:
                            new_traj[item].remove(traject)
                            #print(traject)
                    # print(match_count(new_traj, tussenstops)[0][0])
                    # print(match_count(new_traj, tussenstops)[1][0])
                    if match_count(new_traj, tussenstops)[0][0] >= 2 and match_count(new_traj, tussenstops)[1][0] in sec_tus:
                        fourth_lst.append(item)
                    else:
                        for traject in new_traj[item]:
                            if traject["name"] == match_count(new_traj, tussenstops)[1][0]:
                                new_traj[item].remove(traject)
                                # print(traject)
                        #print(match_count(new_traj, tussenstops)[1][0])
                        if match_count(new_traj, tussenstops)[0][0] >= 2 and match_count(new_traj, tussenstops)[1][
                            0] in sec_tus:
                            fourth_lst.append(item)
                        else:
                            for traject in new_traj[item]:
                                if traject["name"] == match_count(new_traj, tussenstops)[1][0]:
                                    new_traj[item].remove(traject)
                                    # print(traject)
                            #print(match_count(new_traj, tussenstops)[1][0])
                            if match_count(new_traj, tussenstops)[0][0] >= 2 and match_count(new_traj, tussenstops)[1][
                                0] in sec_tus:
                                fourth_lst.append(item)

        print(fourth_lst)
        new_traj1 = make_trajectory_dict([fourth_lst[0], None], filename)
        new_traj2 = make_trajectory_dict([fourth_lst[1], None], filename)
        fifth_list = {}
        #print(fourth_lst)
        for traject in new_traj1[fourth_lst[0]]:
            #print(traject)
            if traject["name"] not in eindlijst_trajecten and traject["name"] in sec_tus:
                fifth_list[fourth_lst[0]] = [traject]

        sixth_list = {}
        for traject in new_traj2[fourth_lst[1]]:
            #print(traject)
            if traject["name"] not in eindlijst_trajecten and traject["name"] in sec_tus:
                sixth_list[fourth_lst[1]] = [traject]

        #print(fifth_list)
        #print(sixth_list)
        if match_count(fifth_list, tussenstops)[1] == match_count(sixth_list, tussenstops)[1]:

            eindlijst_trajecten.append(match_count(fifth_list, tussenstops)[1][0])
            running = False

        elif match(match_count(fifth_list, tussenstops)[1], match_count(sixth_list, tussenstops)[1], True) == False:
            eindlijst_trajecten.append(match_count(fifth_list, tussenstops)[1][0])
            eindlijst_trajecten.append(match_count(sixth_list, tussenstops)[1][0])
            print(match_count(fifth_list, tussenstops)[1][0])
            print(match_count(sixth_list, tussenstops)[1][0])
            running = False
        else:
            eindlijst_trajecten.append(match_count(fifth_list, tussenstops)[1][0])
            eindlijst_trajecten.append(match_count(sixth_list, tussenstops)[1][0])
            station5 = aparte_stations(match_count(fifth_list, tussenstops)[1][0])
            station6 = aparte_stations(match_count(sixth_list, tussenstops)[1][0])

            if station5[0] in fourth_lst and station6[0] in fourth_lst:
                third_lst = [station5[1], station6[1]]
            if station5[1] in fourth_lst and station6[0] in fourth_lst:
                third_lst = [station5[0], station6[1]]
            if station5[0] in fourth_lst and station6[1] in fourth_lst:
                third_lst = [station5[1], station6[0]]
            if station5[1] in fourth_lst and station6[1] in fourth_lst:
                third_lst = [station5[0], station6[0]]
    return eindlijst_trajecten

