# -*- coding: utf-8 -*-
"""
Web Scraping 

@author: bhmgerritsen
@date: Fall 2018
@rev: 1.0
"""

from bs4 import BeautifulSoup as bs
import requests as rq
import time

import re


# Ok response from request ...
OK_CODE = 200

KEYWORDS = ['/trajecten/']


RE_PATTERNS = ['/vertrektijden/']


def get_data_from_web(host, path='', q_string=''):
    """compose a full url and send request"""

    # do some checking first ...
    assert host is not None, 'specify valid host'

    # now compose the url ...
    url = r'http' + '://' + host + path

    # query string specified ?
    if q_string:
        url += r'?' + q_string

    # send the request ...
    # print('GET {:s} :'. format(url))
    page = rq.get(url)

    # check the response code ...
    if page.status_code == OK_CODE:
        pretty_content = bs(page.content, 'html.parser').prettify()
    else:
        print('Bad response to request: {:s}'.format(url))

    return pretty_content


def filter_data_content_by_keyword(content):
    """filter data by keyword list"""
    trajecten = {}
    key = '/trajecten/'

    for ln in range(len(content.splitlines()) - 1):
            # print(ln)
        if key in content.splitlines()[ln]:
            #print(trajecten[content.splitlines()[ln+1][7:]])
            if "'s-Hertogenbosch" in content.splitlines()[ln + 1][7:]:
                trajecten[content.splitlines()[ln + 1][10:]] = content.splitlines()[ln][15:-2]
            else:

                trajecten[content.splitlines()[ln + 1][7:]] = content.splitlines()[ln][15:-2]

    return trajecten


def filter_data_content_by_regex(content, regex_dict, eindstations):
    """filter data by re's"""
    tussenstops = []
    for key in regex_dict:
        hits = 0
        # print('... searching for keyword: {:s} ...'.format(key))
        for ln in range(len(content.splitlines()) - 1):

            if key in content.splitlines()[ln]:
                if "'s-Hertogenbosch" in content.splitlines()[ln+1][9:]:
                    tussenstops.append(content.splitlines()[ln + 1][12:])
                elif "'t Harde" in content.splitlines()[ln+1][9:]:
                    tussenstops.append(content.splitlines()[ln+1][10:])
                else:
                    tussenstops.append(content.splitlines()[ln + 1][9:])
    return tussenstops


def trajecten(trajects):
    lst = []
    time1 = time.time()
    point = ""
    for eindstation in trajects:
        stations = {}
        print(f'eindstations zijn {eindstation}')


        my_content = get_data_from_web(r'rijdendetreinen.nl', \
                                       path=trajects[eindstation])
        tussenstops = filter_data_content_by_regex(my_content, RE_PATTERNS, eindstation)
        stations["name"] = eindstation
        stations["tussenstops"] = tussenstops
        point += "."
        lst.append(stations)
    time2 = time.time()
    print(f'Running time = {time2 - time1}')
    return lst


# run the main programmme ...


def run_main():
    my_content = get_data_from_web(r'rijdendetreinen.nl', \
                                   path=r'/trajecten')  # q_string= f'page={page_nr}')
    alle_stations = trajecten(filter_data_content_by_keyword(my_content))
    # filter_data_content_by_regex(my_content, RE_PATTERNS)

    del my_content
    return alle_stations

