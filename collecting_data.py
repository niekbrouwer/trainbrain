import requests
import math
import pandas as pd
import csv
import numpy
import datetime
import difflib
from bs4 import BeautifulSoup


storing_dict = {
    # malfunction index 0
    'ongelukken': ['aanrijding', 'aanrijding met een dier', 'aanrijding met een persoon', 'aanrijding met een voertuig',
                   'beschadigd spoorviaduct', 'beschadigde overweg', 'beschadigde spoorbrug',
                   'ontspoorde goederentrein', 'ontspoorde trein'],

    # malfunction index 1
    'infrastructuurproblemen': ['beschadigde bovenleiding', 'defect spoor', 'defect wissel', 'defecte bovenleiding',
                                'defecte overweg',
                                'defecte spoorbrug', 'grote sein- en wisselstoring', 'overwegstoring',
                                'sein-en overwegstoring',
                                'sein-en wisselstoring', 'seinstoring', 'storing in de bediening van seinen',
                                'storing in een tunnel',
                                'stroomstoring', 'wisselstoring'],

    # malfunction index 2
    'weersinvloeden': ['aangepaste dienstregeling', 'blikseminslag', 'de verwachte weersomstandigheden',
                       'de weersomstandigheden',
                       'gladde sporen', 'harde wind op de Hogesnelheidslijn', 'ijzelvorming aan de bovenleiding',
                       'rijp aan de bovenleiding',
                       'stormschade', 'wateroverlast'],

    # malfunction index 3
    'personeelsinzet': ['acties in het buitenland', 'problemen met de personeelsinzet', 'staking'],

    # malfunction index 4
    'materieelproblemen': ['beperkingen in de materieelinzet', 'brand in een trein', 'de inzet van ander materieel',
                           'defecte trein',
                           'defecte treinen', 'gestrande trein', 'rook in een trein'],

    # malfunction index 5
    'onbekende oorzaak': ['meerdere verstoringen', 'nog onbekende oorzaak', 'technisch onderzoek', 'treinstoring',
                          'versperring'],

    # malfunction index 6
    'logistieke problemen': ['eerdere verstoring', 'problemen op het spoor in het buitenland', 'te grote vertraging',
                             'te veel vertraging in het buitenland', 'verstoring elders'],

    # malfunction index 7
    'werkzaamheden': ['de bouw van een tunnel', 'herstelwerkzaamheden', 'onverwachte werkzaamheden',
                      'spoedreparatie aan het spoor',
                      'uitloop van werkzaamheden', 'werkzaamheden', 'werkzaamheden elders'],

    # malfunction index 8
    'externe invloeden': ['bermbrand', 'boom op het spoor', 'brandmelding', 'dier op het spoor', 'evenement',
                          'grote drukte',
                          'het onschadelijk maken van een WO-II bom', 'hinder op het spoor', 'inzet van de brandweer',
                          'inzet van de politie',
                          'inzet van hulpdiensten', 'koperdiefstal', 'mensen langs het spoor', 'mensen op het spoor',
                          'om veiligheidsredenen',
                          'persoon langs het spoor', 'persoon op het spoor', 'politieonderzoek', 'rommel op het spoor',
                          'Thialf-schaatsen',
                          'vandalisme', 'voertuig op het spoor', 'voorwerp in de bovenleiding']
}


def find_page_number():
    url = "https://www.rijdendetreinen.nl/storingen?"
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    num_malfunc = soup.find(id="w0").b.next_sibling.next_sibling.text
    num_malfunc = int(num_malfunc[:2] + num_malfunc[3:])
    return math.ceil(num_malfunc / 20)


def create_dataset():
    daytuples = []
    for i in range(1, 54):
        for j in range(1, 8):
            daytuples.append((i, j))

    trajectory_list = []
    with open("tussenstations.csv") as f:
        zeros = numpy.zeros(9)

        reader = csv.DictReader(f)
        for row in reader:
            stations = row['name'].split(" - ")
            trajectory_list.append(stations[0] + '-' + stations[1])
            trajectory_list.append(stations[1] + '-' + stations[0])

        malfunc_data = pd.DataFrame(index=trajectory_list, columns=daytuples)
        for i in range(len(trajectory_list)):
            for j in range(len(daytuples)):
                malfunc_data.iloc[i][j] = zeros

    return malfunc_data, trajectory_list


def convert_date(year, month, day):
    y, w, d = datetime.date(year, month, day).isocalendar()
    return (w, d)


def date_to_week(date):
    datelist = date.split()
    months = ['januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december']
    month = months.index(datelist[1])+1
    year = int(datelist[2])
    month = month
    day = int(datelist[0])
    return convert_date(year, month, day)


def trajectory_exception(trajectory, trajectory_list):
    if '(HSL)' in trajectory:
        trajectory = trajectory.replace(' (HSL)', '')
    if 'Den Haag C' in trajectory:
        trajectory = trajectory.replace('Den Haag C', 'Den Haag Centraal')
    # if trajectory.count('-') > 1:
    #     multiple_stations = trajectory.split('-')
    #     trajectory = multiple_stations[0] + '-' + multiple_stations[-1]
    #     # trajectory = None
    #     # return trajectory

    try:
        trajectory_match = difflib.get_close_matches(trajectory, trajectory_list, n=1)[0]
        trajectory_separate = trajectory.split('-')
        if trajectory_separate[0] in trajectory_match and trajectory_separate[1] in trajectory_match:
            trajectory = trajectory_match
        else:
            trajectory = None
        return trajectory
    except IndexError:
        trajectory = None
        return trajectory


def filling_dataset(year):
    page_numbers = find_page_number()
    my_dataset, all_trajectories = create_dataset()
    i = 1
    nonecount = 0
    stop = False
    while i <= page_numbers or stop is False:
        # create soup
        url = "https://www.rijdendetreinen.nl/storingen?page={}".format(i)
        r = requests.get(url)
        soup = BeautifulSoup(r.content, 'html.parser')

        # add malfunctions to data set
        for malfunction in soup.body.find_all('div', class_ = None):
            # find date in correct format
            date_list = malfunction.a.get('href').split('-')

            # skip malfunction if not correct year yet
            if int(date_list[3]) > year:
                continue

            # stop iteration if not correct year anymore
            if int(date_list[3]) < year:
                stop = True
                break

            date = date_list[1] + ' ' + date_list[2] + ' ' + date_list[3]
            date = date_to_week(date)

            # find malfunction index
            malfunction_type = malfunction.em.text.strip()
            if malfunction_type in storing_dict['ongelukken']:
                malfunction_index = 0
            if malfunction_type in storing_dict['infrastructuurproblemen']:
                malfunction_index = 1
            if malfunction_type in storing_dict['weersinvloeden']:
                malfunction_index = 2
            if malfunction_type in storing_dict['personeelsinzet']:
                malfunction_index = 3
            if malfunction_type in storing_dict['materieelproblemen']:
                malfunction_index = 4
            if malfunction_type in storing_dict['onbekende oorzaak']:
                malfunction_index = 5
            if malfunction_type in storing_dict['logistieke problemen']:
                malfunction_index = 6
            if malfunction_type in storing_dict['werkzaamheden']:
                malfunction_index = 7
            if malfunction_type in storing_dict['externe invloeden']:
                malfunction_index = 8

            # find trajectory
            trajectory = malfunction.span.text
            if '; ' in trajectory:
                trajectory_list = trajectory.split('; ')
                for traj in trajectory_list:
                    traj = trajectory_exception(traj, all_trajectories)

                    #print(traj)

                    if traj is None:
                        nonecount += 1
                        continue

                    my_list = list(my_dataset.at[traj, date])
                    my_list[malfunction_index] += 1
                    my_dataset.at[traj, date] = my_list

            elif trajectory.count('-') > 1:
                multiple_stations = trajectory.split('-')

                for j in range(len(multiple_stations)-1):
                    t = multiple_stations[j] + '-' + multiple_stations[j+1]

                    trajec = trajectory_exception(t, all_trajectories)

                    if trajec is None:
                        nonecount += 1
                        continue

                    my_list = list(my_dataset.at[trajec, date])
                    my_list[malfunction_index] += 1
                    my_dataset.at[trajec, date] = my_list

            elif trajectory.count('-') == 0:
                nonecount += 1
                continue

            else:
                trajectory = trajectory_exception(trajectory, all_trajectories)

                if trajectory is None:
                    nonecount += 1
                    continue

                my_list = list(my_dataset.at[trajectory, date])
                my_list[malfunction_index] += 1
                my_dataset.at[trajectory, date] = my_list

        if stop:
            break

        # increase page number
        i += 1

    return my_dataset, nonecount


def create_dataset_files(year):
    dataframe = filling_dataset(year)[0]
    filename = f'dataset_2.0_{str(year)}.csv'
    dataframe.to_csv(filename)

create_dataset_files(2019)
create_dataset_files(2018)
create_dataset_files(2017)


