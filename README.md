<!--
*** Thanks for checking out this README Template. If you have a suggestion that would
*** make this better, please fork the repo and create a pull request or simply open
*** an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->





<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
  * [Usage](#usage)
  * [License](#license)
  * [Referencing](#reference)
  * [Contact](#contact)
  




<!-- ABOUT THE PROJECT -->
## About The Project

In this project you will find the code accompanying a groupproject I did during the minor computer science at the TU Delft. The report of this research can be requested via the contact details below. 

### Built With
The code is written in Python. The webscraping is done using the package BeatifulSoup and Requests.

## Usage
The application is used by running the main.py file.

## Referencing
The Partial or complete usage of this code is not allowed without contacting the developers.


<!-- CONTACT -->
## Contact

Niek Brouwer - niek.brouwer@outlook.com