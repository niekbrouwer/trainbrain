import datetime
import pytest
import numpy as np



def convert_date(year, month, day):
    tuple = datetime.date(year, month, day).isocalendar()
    return tuple


#print(convert_date(2017, 7, 6))
#print(convert_date(2018, 10, 31))


def date_weekrange(year, month, day):
    year, weeknumber, weekday = convert_date(year, month, day)
    if weeknumber == 50:
        if datetime.date(year, 12, 31).isocalendar()[1] == 53:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, 52, weekday), (year, 53, weekday), (year + 1, 1, weekday)]
        else:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year, 51, weekday), (year, 52, weekday), (year + 1, 1, weekday), (year + 1, 2, weekday)]

    if weeknumber == 51:
        if datetime.date(year, 12, 31).isocalendar()[1] == 53:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, 53, weekday), (year + 1, 1, weekday), (year + 1, 2, weekday)]
        else:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year, 52, weekday), (year + 1, 1, weekday), (year + 1, 2, weekday), (year + 1, 3, weekday)]

    if weeknumber == 52:
        if datetime.date(year, 12, 31).isocalendar()[1] == 53:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year + 1, 1, weekday), (year + 1, 2, weekday), (year + 1, 3, weekday)]
        else:
            return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year + 1, 1, weekday), (year + 1, 2, weekday), (year + 1, 3, weekday), (year + 1, 4, weekday)]

    if weeknumber == 53:
        return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                    (year + 1, 1, weekday), (year + 1, 2, weekday), (year + 1, 3, weekday), (year + 1, 4, weekday)]

    if weeknumber == 1:
        if datetime.date(year - 1, 12, 31).isocalendar()[1] == 53:
            return [(year - 1, 50, weekday), (year - 1, 51, weekday), (year - 1, 52, weekday), (year - 1, 53, weekday), (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday), (year, weeknumber + 4, weekday)]
        else:
            return [(year - 1, 49, weekday), (year - 1, 50, weekday), (year - 1, 51, weekday), (year - 1, 52, weekday), (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday), (year, weeknumber + 4, weekday)]

    if weeknumber == 2:
        if datetime.date(year - 1, 12, 31).isocalendar()[1] == 53:
            return [(year - 1, 51, weekday), (year - 1, 52, weekday), (year - 1, 53, weekday), (year, 1, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]
        else:
            return [(year - 1, 50, weekday), (year - 1, 51, weekday), (year - 1, 52, weekday), (year, 1, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]

    if weeknumber == 3:
        if datetime.date(year - 1, 12, 31).isocalendar()[1] == 53:
            return [(year - 1, 52, weekday), (year - 1, 53, weekday), (year, 1, weekday), (year, 2, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]
        else:
            return [(year - 1, 51, weekday), (year - 1, 52, weekday), (year, 1, weekday), (year, 2, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]

    if weeknumber == 4:
        if datetime.date(year - 1, 12, 31).isocalendar()[1] == 53:
            return [(year - 1, 53, weekday), (year - 1, 1, weekday), (year, 2, weekday), (year, 3, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]
        else:
            return [(year - 1, 52, weekday), (year - 1, 1, weekday), (year, 2, weekday), (year, 3, weekday),
                    (year, weeknumber, weekday),
                    (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday),
                    (year, weeknumber + 4, weekday)]

    else:
        return [(year, weeknumber - 4, weekday), (year, weeknumber - 3, weekday), (year, weeknumber - 2, weekday), (year, weeknumber - 1, weekday), (year, weeknumber, weekday),
                (year, weeknumber + 1, weekday), (year, weeknumber + 2, weekday), (year, weeknumber + 3, weekday), (year, weeknumber + 4, weekday)]


def test_convert_date():
    number = (48, 4)
    assert convert_date(2019, 12, 1) is number



def iso_to_gregorian(iso_year, iso_week, iso_day):
    fourth_jan = datetime.date(iso_year, 1, 4)
    _, fourth_jan_week, fourth_jan_day = fourth_jan.isocalendar()
    datum = fourth_jan + datetime.timedelta(days=iso_day - fourth_jan_day, weeks=iso_week - fourth_jan_week)
    return datum.year, datum.month, datum.day


#print(iso_to_gregorian(2019, 8, 5))





