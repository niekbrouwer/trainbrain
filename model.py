import pandas
import numpy as np
from Developer_code.date_converter import convert_date, date_weekrange
import csv


def load_data(file1, file2, file3, date, trajects):
    data1 = pandas.read_csv(file1, index_col=0)
    data2 = pandas.read_csv(file2, index_col=0)
    data3_i = pandas.read_csv(file3, index_col=0)
    data3 = data3_i.iloc[:, 0:len(data3_i.iloc[1, :]) // 2]
    new_trajects = []
    for traject in trajects:
        splitted = traject.split(" - ")
        combined = splitted[0] + "-" + splitted[1]
        new_trajects.append(combined)

    # print(new_trajects)
    empty_array = []
    # print(date)
    for traject in new_trajects:
        traject_array = np.zeros(9)
        for day in date:
            # print(day[1:])
            data_per_day1 = data1[str(day[1:])]
            data_per_day2 = data2[str(day[1:])]
            storingen_per_day1 = data_per_day1[traject][1:-1].split(". ")
            storingen_per_day2 = data_per_day2[traject][1:-1].split(". ")

            if str(day[1:]) in data3:
                data_per_day3 = data3[str(day[1:])]
                storingen_per_day3 = data_per_day3[traject][1:-1].split(". ")
            else:
                storingen_per_day3 = [0]

            for i in range(len(storingen_per_day1)):
                traject_array[i] += float(storingen_per_day1[i])

            for i in range(len(storingen_per_day2)):
                traject_array[i] += float(storingen_per_day2[i])

            for i in range(len(storingen_per_day3)):
                traject_array[i] += float(storingen_per_day3[i])
        empty_array.append(traject_array)
        # print(traject_array)
    return empty_array


file1 = "dataset_3.0_2017.csv"
file2 = "dataset_3.0_2018.csv"
file3 = "dataset_3.0_2019.csv"


def generate_dates(date):
    year, month, day = date.split("-")
    date = date_weekrange(int(year), int(month), int(day))
    # print(date)
    return date


def weights(array):
    # print(array)
    weight_list = [0.103, 0.276, 0.103, 0.034, 0.103, 0.034, 0.207, 0.138, 0.034]
    new_lst = []
    for traject in array:
        weighted_array = weight_list * traject
        new_lst.append(sum(weighted_array))

    return new_lst


def prob(array, trajecten, filename):
    print(array)
    # print(trajecten)
    new_traj = []
    for traject in trajecten:
        splitted = traject.split(" - ")
        combined = splitted[0] + "-" + splitted[1]
        new_traj.append(combined)

    # print(new_traj)
    all = []

    for traject in new_traj:
        with open(filename) as data:
            next(data)
            for line in data:
                if traject in line:
                    freq = float(line.split(",")[1])
                    if freq != 0:
                        all.append(freq)
                    else:
                        all.append(50)

    return all, array


def generate_chance(all, array):
    all = np.array(all)
    array = np.array(array)
    return f'The chance you are delayed is {(max(array / all)*100).__round__(2)} %'
