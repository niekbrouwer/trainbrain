from bs4 import BeautifulSoup as bs
import requests as rq

# Ok response from request ...
OK_CODE = 200


KEYWORDS = [
    r'StopHalte'
]

def get_data_from_web(host, path='', q_string=''):
    """compose a full url and send request"""

    # do some checking first ...
    assert host is not None, 'specify valid host'

    # now compose the url ...
    url = r'http' + '://' + host + path

    # query string specified ?
    if q_string:
        url += r'?' + q_string

    # send the request ...
    print('GET {:s} :'.format(url))
    page = rq.get(url)

    # check the response code ...
    if page.status_code == OK_CODE:
        pretty_content = bs(page.content, 'html.parser').prettify()
    else:
        print('Bad response to request: {:s}'.format(url))
    return pretty_content


def generate_path(origin, destination, date, time, departure=True, extraInterchangeTime=0):
    if departure == True:
        path = r'/reisadvies/station-' + origin + r'/station-' + destination + r'/vertrek/' + date + r'T' + time + r'?bus=off&metro=off&tram=off&veerboot=off&extraInterchangeTime=' + str(
            extraInterchangeTime)

    else:
        path = r'/reisadvies/station-' + origin + r'/station-' + destination + r'/aankomst/' + date + r'T' + time + r'?bus=off&metro=off&tram=off&veerboot=off&extraInterchangeTime=' + str(
            extraInterchangeTime)


    return path


def show_intermediate_stops(content):
    #print('... searching for intermediate stops...')
    hits = 0
    for ln in content.splitlines():
        if 'StopHalte' in ln:
            hits = ln
            break
    return hits


# run the main programmme ...
def run(INPUT):
    traj = INPUT[0] + ' - ' + INPUT[1]

    if 'Utrecht Maliebaan' in traj:
        traj = traj.replace('Utrecht Maliebaan', 'Maliebaan Utrecht')
    if 'ü' in traj:
        traj = traj.replace('ü', 'u')
    if '(M)' in traj:
        traj = traj.replace('(M)', '')
    if 'Alphen aan den Rijn' in traj:
        traj = traj.replace('Alphen aan den Rijn', 'Alphen a d Rijn')
    if 'Brussel-Zuid' in traj:
        traj = traj.replace('Brussel-Zuid', 'Brussel Zuid Midi')
    if 'Arnhem' in traj:
        traj = traj.replace('Arnhem Centraal', 'Arnhem')
    if 'Eindhoven' in traj:
        traj = traj.replace('Eindhoven Centraal', 'Eindhoven')
    if 'Amersfoort' in traj:
        traj = traj.replace('Amersfoort Centraal', 'Amersfoort')
    if "'s-Hertogenbosch" in traj:
        traj = traj.replace("'s-Hertogenbosch", "Hertogenbosch")
    if 'Westf.' in traj:
        traj = traj.replace('(Westf.)', '')
        traj = traj.replace('Munster', 'Muenster')

    INPUT[0], INPUT[1] = traj.split(' - ')

    station1 = INPUT[0].lower().split()

    station2 = INPUT[1].lower().split()

    station1_1 = str(station1[0])
    station2_1 = str(station2[0])

    if station1_1 == "hertogenbosch":
        station1_1 = "s-" + "hertogenbosch"

    if station2_1 == "hertogenbosch":
        station2_1 = "s-" + "hertogenbosch"

    for station in station1[1:]:
        inter = str('-') + station
        station1_1 += inter


    for station in station2[1:]:
        inter = str('-') + station
        station2_1 += inter

    path = generate_path(station1_1, station2_1, INPUT[3], INPUT[2])


    my_content = get_data_from_web(r'9292.nl', \
                                   path=path)

    return show_intermediate_stops(my_content)


# def test_generate_path():
#     path = r'/reisadvies/station-delft/station-amsterdam-centraal/vertrek/2019-11-28T1121?extraInterchangeTime=0'
#     path2 = r'/reisadvies/station-delft/station-amsterdam-centraal/vertrek/2019-11-28T1121?extraInterchangeTime=5'
#     path3 = r'/reisadvies/station-delft/station-amsterdam-centraal/aankomst/2019-11-28T1121?extraInterchangeTime=0'
#     path4 = r'/reisadvies/station-delft/station-amsterdam-centraal/aankomst/2019-11-28T1121?extraInterchangeTime=5'
#
#     assert path == generate_path('delft', 'amsterdam-centraal', '2019-11-28', '1121', departure=True,
#                                  extraInterchangeTime=0)
#     assert path2 == generate_path('delft', 'amsterdam-centraal', '2019-11-28', '1121', departure=True,
#                                   extraInterchangeTime=5)
#     assert path3 == generate_path('delft', 'amsterdam-centraal', '2019-11-28', '1121', departure=False,
#                                   extraInterchangeTime=0)
#     assert path4 == generate_path('delft', 'amsterdam-centraal', '2019-11-28', '1121', departure=False,
#                                   extraInterchangeTime=5)
#
#
# test_generate_path()
